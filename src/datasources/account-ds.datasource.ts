import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'accountDS',
  connector: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'vicky',
  password: 'Pooja@1996',
  database: 'vicky'
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class AccountDsDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'accountDS';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.accountDS', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
